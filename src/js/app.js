import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

const burgerButton = document.querySelector('.header__burger-menu-btn');
const burgerLines = document.querySelectorAll('.header__burger-menu-line');
const burgerMenu = document.querySelector('.header__burger-menu');

window.addEventListener('click', e => {

    const lines = e.target.hasAttribute('data-line');
    const burgerMenuItems = e.target.hasAttribute('data-menu-item');
    const burgerMenuLinks = e.target.hasAttribute('data-menu-link');

    if(e.target === burgerButton || lines) {
        burgerLines.forEach( item => item.classList.toggle('active'));
        burgerMenu.classList.toggle('active');
    }

    if(e.target !== burgerMenu && e.target !== burgerButton && !lines && !burgerMenuItems && !burgerMenuLinks) {
        burgerLines.forEach( item => item.classList.remove('active'));
        burgerMenu.classList.remove('active');
    }
})